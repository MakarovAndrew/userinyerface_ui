package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.apache.commons.lang3.RandomStringUtils.*;

public class RandomUtils {
    public static String randomAlphaNumericString(int length) {
        return randomAlphanumeric(length);
    }

    public static String randomNumericString(int length) {
        return randomNumeric(length);
    }

    public static String randomAlphabeticString(int length) {
        return randomAlphabetic(length);
    }

    public static int randomNumber(int max) {
        return org.apache.commons.lang3.RandomUtils.nextInt(0, max);
    }

    public static <T> T getRandomElementFromList (List<T> list) {
        return list.get(randomNumber(list.size()));
    }

    public static <T> List<T> getListOfRandomElementsFromList(List<T> list, int countOfRandomElements) {
        List<T> randomList = new ArrayList<>(list);
        Collections.shuffle(randomList);
        return countOfRandomElements > randomList.size() ? randomList : randomList.subList(0, countOfRandomElements);
    }
}