package userinterface.forms;

import aquality.selenium.forms.Form;
import org.openqa.selenium.By;

public abstract class BaseForm extends Form {

    protected BaseForm(By locator, String name) {
        super(locator, name);
    }

    public boolean isDisplayed() {
        return this.state().isDisplayed();
    }

    public boolean waitForDisplayed() {
        return this.state().waitForDisplayed();
    }
}
