package userinterface.forms;

import aquality.selenium.elements.interfaces.ILabel;
import org.openqa.selenium.By;

public class CardsForm extends BaseForm {

    private final ILabel timer = getElementFactory().getLabel(By.cssSelector(".timer.timer--white.timer--center"), "Timer");
    private final static String REGEXP_CUT_OFF_SECONDS = ".{3}$";

    public CardsForm() {
        super(By.cssSelector(".pagination.pagination--center"), "Cards Page");
    }

    public String getTimerValue() {
        return timer.getText().replaceFirst(REGEXP_CUT_OFF_SECONDS, "");
    }
}