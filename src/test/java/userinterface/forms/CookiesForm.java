package userinterface.forms;

import aquality.selenium.elements.interfaces.IButton;
import org.openqa.selenium.By;

public class CookiesForm extends BaseForm {

    private final IButton acceptCookiesButton = getElementFactory().getButton(By.cssSelector(".button.button--solid.button--transparent"), "Accept Cookies");

    public CookiesForm() {
        super(By.className("cookies"), "Cookies Banner");
    }

    public void clickAcceptCookiesButton() {
        acceptCookiesButton.click();
    }
}