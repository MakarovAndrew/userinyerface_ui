package userinterface.forms;

import aquality.selenium.elements.interfaces.IButton;
import aquality.selenium.elements.interfaces.ILabel;
import org.openqa.selenium.By;

public class HelpForm extends BaseForm {

    private final IButton sendToBottomButton = getElementFactory().getButton(By.cssSelector(".help-form__send-to-bottom-button"), "Send to bottom");
    private final ILabel helpFormTitle = getElementFactory().getLabel(By.className("help-form__title"), "The Help From title");

    public HelpForm() {
        super(By.className("help-form"), "Help form");
    }

    public void clickSendToBottomButton() {
        sendToBottomButton.click();
    }

    public boolean isHelpFormTitleNotDisplayed() {
        return helpFormTitle.state().waitForNotDisplayed();
    }
}