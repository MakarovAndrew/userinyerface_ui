package userinterface.forms;

import aquality.selenium.elements.interfaces.*;
import static utils.RandomUtils.*;
import org.openqa.selenium.By;
import java.util.List;

public class TheFirstCard extends BaseForm {

    private final ITextBox passwordTextBox = getElementFactory().getTextBox(By.xpath("//input[@placeholder='Choose Password']"), "Password");
    private final ITextBox emailTextBox = getElementFactory().getTextBox(By.xpath("//input[@placeholder='Your email']"), "Email");
    private final ITextBox domainTextBox = getElementFactory().getTextBox(By.xpath("//input[@placeholder='Domain']"), "Domain");
    private final IComboBox theFirstLevelDomainComboBox = getElementFactory().getComboBox(By.cssSelector(".dropdown.dropdown--gray"), "The First level domain");
    private final ICheckBox termsOfUseCheckBox = getElementFactory().getCheckBox(By.className("checkbox__box"), "Terms of use");
    private final IButton theNextButton = getElementFactory().getButton(By.className("button--secondary"), "Next");

    public TheFirstCard() {
        super(By.className("login-form"), "The First Card");
    }

    private List<ILabel> getListOfTheFirstLevelDomains() {
        return getElementFactory().findElements(By.xpath("//div[@class='dropdown__list-item']"), ILabel.class);
    }

    public void chooseTheFirstLevelDomain() {
        clickTheFirstLevelDomainComboBox();

        List<ILabel> listOfTheFirstLevelDomains = getListOfTheFirstLevelDomains();

        getRandomElementFromList(listOfTheFirstLevelDomains).click();
    }

    public void inputEmail(String email) {
        emailTextBox.clearAndType(email);
    }

    public void inputDomain(String domain) {
        domainTextBox.clearAndType(domain);
    }

    public void inputPassword(String password) {
        passwordTextBox.clearAndType(password);
    }

    public void checkTermsOfUseCheckBox() {
        termsOfUseCheckBox.click();
    }

    public void clickTheNextButton() {
        theNextButton.click();
    }

    public void clickTheFirstLevelDomainComboBox() {
        theFirstLevelDomainComboBox.click();
    }
}