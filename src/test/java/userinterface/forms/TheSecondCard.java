package userinterface.forms;

import aquality.selenium.elements.interfaces.*;
import org.openqa.selenium.*;
import static utils.RandomUtils.*;
import java.util.List;

public class TheSecondCard extends BaseForm {

    private final IButton uploadButton = getElementFactory().getButton(By.className("avatar-and-interests__upload-button"), "Upload");
    private final IButton theNextButton = getElementFactory().getButton(By.xpath("//button[contains(@class,'button--stroked')]"), "Next");
    private final ICheckBox unselectAllCheckBox = getElementFactory().getCheckBox(By.xpath("//span[text()='Unselect all']/../span[@class]"), "Unselect All");

    public TheSecondCard() {
        super(By.className("avatar-and-interests__form"), "The Second Card");
    }

    private List<ICheckBox> getListOfInterests() {
        return getElementFactory().findElements(By.xpath("//label[not(contains(@for,'electall'))]"), ICheckBox.class);
    }

    public void pickRandomInterests(int count) {
        List<ICheckBox> listOfInterests = getListOfInterests();

        unselectAllCheckBox.click();

        for (ICheckBox randomInterest: getListOfRandomElementsFromList(listOfInterests, count)) {
            randomInterest.click();
        }
    }

    public void clickUploadButton() {
        uploadButton.click();
    }

    public void clickTheNextButton() {
        theNextButton.click();
    }
}