package userinterface.forms;

import org.openqa.selenium.By;

public class TheThirdCard extends BaseForm {

    public TheThirdCard() {
        super(By.className("personal-details__form"), "The Third Card");
    }
}