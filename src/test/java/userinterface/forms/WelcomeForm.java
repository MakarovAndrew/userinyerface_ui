package userinterface.forms;

import aquality.selenium.elements.interfaces.ILink;
import org.openqa.selenium.By;

public class WelcomeForm extends BaseForm {

    private final ILink cardsPageLink = getElementFactory().getLink(By.className("start__link"), "Cards Page");

    public WelcomeForm() {
        super(By.className("start__button"), "Welcome Page");
    }

    public void clickCardsPageLink() {
        cardsPageLink.click();
    }
}