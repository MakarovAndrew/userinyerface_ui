package userinterface.tests;

import aquality.selenium.browser.AqualityServices;
import aquality.selenium.core.utilities.ISettingsFile;
import aquality.selenium.core.utilities.JsonSettingsFile;
import aquality.selenium.elements.interfaces.IElementFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest {

    private final static ISettingsFile configFile = new JsonSettingsFile("config.json");
    protected final static String BASE_URL = configFile.getValue("/url").toString();
    private final static ISettingsFile testDataFile = new JsonSettingsFile("testdata.json");
    protected final static String IMAGE_URL = testDataFile.getValue("/imageUrl").toString();
    protected final IElementFactory elementFactory;

    protected BaseTest() {
        elementFactory = AqualityServices.getElementFactory();
    }

    @BeforeMethod
    protected void beforeMethod() {
        AqualityServices.getBrowser().maximize();
        AqualityServices.getBrowser().goTo(BASE_URL);
    }

    @AfterMethod
    public void afterTest() {
        AqualityServices.getBrowser().quit();
    }
}