package userinterface.tests;

import aquality.selenium.core.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import userinterface.forms.*;
import utils.NativeDialogHandler;
import java.time.Duration;
import static utils.RandomUtils.*;

public class CardsFormTest extends BaseTest {

    private final static int WAIT_BEFORE_UPLOAD_DIALOG_OPEN = 5;
    private final static int WAIT_AFTER_UPLOAD_DIALOG_CLOSE = 10;
    private final static int EMAIL_LENGTH = 8;
    private final static int DOMAIN_LENGTH = 3;
    private final static int VALID_PASSWORD_TAIL_LENGTH = 7;
    private final static int COUNT_OF_INTERESTS = 3;
    private final static String RANDOM_EMAIL = randomAlphaNumericString(EMAIL_LENGTH).toLowerCase();
    private final static String RANDOM_DOMAIN = randomAlphaNumericString(DOMAIN_LENGTH).toLowerCase();

    private final WelcomeForm welcomeForm = new WelcomeForm();
    private final TheFirstCard theFirstCard = new TheFirstCard();
    private final TheSecondCard theSecondCard = new TheSecondCard();
    private final TheThirdCard theThirdCard = new TheThirdCard();

    public String makeValidPassword(int passwordTailLength) {

        return
                RANDOM_EMAIL.charAt(randomNumber(EMAIL_LENGTH)) +
                randomAlphabeticString(1).toUpperCase() +
                randomNumericString(1) +
                randomAlphaNumericString(passwordTailLength);
    }

    @Test (description = "Test Case 1: checks if the third page is opening")
    public void checkTheThirdCardAvailability() {
        Logger.getInstance().info("Performing STEP 1: Navigate to the home page...");
        Assert.assertTrue(welcomeForm.waitForDisplayed(), welcomeForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 2: Click the link to navigate the next page...");
        welcomeForm.clickCardsPageLink();
        Assert.assertTrue(theFirstCard.waitForDisplayed(), theFirstCard.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 3: Input random valid credentials, accept the terms of use and click \"next\" button...");
        theFirstCard.inputEmail(RANDOM_EMAIL);
        theFirstCard.inputDomain(RANDOM_DOMAIN);
        theFirstCard.inputPassword(makeValidPassword(VALID_PASSWORD_TAIL_LENGTH));
        theFirstCard.chooseTheFirstLevelDomain();
        theFirstCard.checkTermsOfUseCheckBox();
        theFirstCard.clickTheNextButton();
        Assert.assertTrue(theSecondCard.waitForDisplayed(), theSecondCard.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 4: Choose 3 random interests, upload image, click the \"Next\" button....");
        theSecondCard.pickRandomInterests(COUNT_OF_INTERESTS);
        theSecondCard.clickUploadButton();
            theSecondCard.state().waitForNotEnabled(Duration.ofSeconds(WAIT_BEFORE_UPLOAD_DIALOG_OPEN));
                NativeDialogHandler.uploadImage(BaseTest.IMAGE_URL);
            theSecondCard.state().waitForNotEnabled(Duration.ofSeconds(WAIT_AFTER_UPLOAD_DIALOG_CLOSE));
        theSecondCard.clickTheNextButton();
        Assert.assertTrue(theThirdCard.waitForDisplayed(), theThirdCard.getName() + " is not opened!");
    }
}