package userinterface.tests;

import aquality.selenium.core.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import userinterface.forms.CardsForm;
import userinterface.forms.CookiesForm;
import userinterface.forms.WelcomeForm;

public class CookiesFormTest extends BaseTest {

    private final WelcomeForm welcomeForm = new WelcomeForm();
    private final CardsForm cardsForm = new CardsForm();
    private final CookiesForm cookiesForm = new CookiesForm();

    @Test (description = "Test Case 3: checks if the Cookies form is being closed")
    public void checkTheCookiesFormIsBeingClosed() {
        Logger.getInstance().info("Performing STEP 1: Navigate to the home page...");
        Assert.assertTrue(welcomeForm.waitForDisplayed(), welcomeForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 2: Click the link to navigate the next page...");
        welcomeForm.clickCardsPageLink();
        Assert.assertTrue(cardsForm.waitForDisplayed(), cardsForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 3: Accept cookies...");
        cookiesForm.clickAcceptCookiesButton();
        Assert.assertFalse(cookiesForm.isDisplayed(), "Cookies Form is not closed!");
    }
}