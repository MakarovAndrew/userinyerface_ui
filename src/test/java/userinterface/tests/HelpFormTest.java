package userinterface.tests;

import aquality.selenium.core.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import userinterface.forms.CardsForm;
import userinterface.forms.HelpForm;
import userinterface.forms.WelcomeForm;

public class HelpFormTest extends BaseTest {

    private final WelcomeForm welcomeForm = new WelcomeForm();
    private final CardsForm cardsForm = new CardsForm();
    private final HelpForm helpForm = new HelpForm();

    @Test (description = "Test Case 2: checks if the Help form is being hidden")
    public void checkTheHelpFormIsBeingHidden() {
        Logger.getInstance().info("Performing STEP 1: Navigate to the Home page...");
        Assert.assertTrue(welcomeForm.waitForDisplayed(), welcomeForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 2: Click the link to navigate the next page...");
        welcomeForm.clickCardsPageLink();
        Assert.assertTrue(cardsForm.waitForDisplayed(), cardsForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 3: Hide the Help form...");
        helpForm.clickSendToBottomButton();
        Assert.assertTrue(helpForm.isHelpFormTitleNotDisplayed(), helpForm.getName() + " is still visible!");
    }
}