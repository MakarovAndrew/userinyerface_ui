package userinterface.tests;

import aquality.selenium.core.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import userinterface.forms.CardsForm;
import userinterface.forms.WelcomeForm;

public class TimerTest extends BaseTest {

    private final WelcomeForm welcomeForm = new WelcomeForm();
    private final CardsForm cardsForm = new CardsForm();

    @Test (description = "Test Case 4: checks if the timer starts from 00:00")
    public void checkTheTimerStartsFromZero() {
        Logger.getInstance().info("Performing STEP 1: Navigate to the home page...");
        Assert.assertTrue(welcomeForm.waitForDisplayed(), welcomeForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 2: Click the link to navigate the next page...");
        welcomeForm.clickCardsPageLink();
        Assert.assertTrue(cardsForm.waitForDisplayed(), cardsForm.getName() + " is not opened!");

        Logger.getInstance().info("Performing STEP 3: Validate timer starts from 00:00...");
        Assert.assertEquals(cardsForm.getTimerValue(), "00:00", "Timer does not start from \"00:00\"!");
    }
}